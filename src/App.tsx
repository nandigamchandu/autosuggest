import React from 'react'

import { ExampleSuggest, ExampleFormikSuggest } from './ExampleAutosuggest'

export const App = () => (
  <>
    <ExampleSuggest />
    <ExampleFormikSuggest />
  </>
)
