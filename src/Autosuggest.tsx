import Downshift, { DownshiftProps } from 'downshift'
import React from 'react'
import { MenuItem, MenuList, IconButton, TextField } from '@material-ui/core'
import { MdClear, MdExpandLess, MdExpandMore } from 'react-icons/md'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  container: { position: 'relative' },

  iconFrame: {
    overflow: 'visible',
    width: 2,
    height: 2,
    position: 'absolute',
    right: 0,
    zIndex: 1,
  },
  icon: { marginLeft: -48 },
  menuList: { maxHeight: '200px', overflow: 'auto' },
})
interface AutoSuggestExtraProps<T> {
  items: T[]
  name: string
  label?: string
  itemLabel: keyof T
  placeholder?: string
  getItemSelected: (item: T, value: string | null) => boolean
}

export type AutoSuggestProps<T> = AutoSuggestExtraProps<T> & DownshiftProps<T>

const Container = React.forwardRef<
  HTMLDivElement,
  React.HTMLAttributes<HTMLDivElement>
>((props, ref) => <div {...props} ref={ref} />)

export function AutoSuggest<T>({
  items,
  name,
  label,
  itemLabel,
  placeholder,
  getItemSelected,
  initialInputValue,
  ...rest
}: AutoSuggestProps<T>) {
  const classes = useStyles()
  return (
    <Downshift initialInputValue={initialInputValue} {...rest}>
      {props => {
        const toggleIcon = props.selectedItem ? (
          <MdClear />
        ) : props.isOpen ? (
          <MdExpandLess />
        ) : (
          <MdExpandMore />
        )

        const toggleAction = props.selectedItem
          ? props.clearSelection
          : props.toggleMenu

        return (
          <Container {...props.getRootProps()} className={classes.container}>
            <div className={classes.iconFrame}>
              <IconButton
                className={classes.icon}
                onClick={_ => toggleAction()}
              >
                {toggleIcon}
              </IconButton>
            </div>
            <TextField
              {...props.getInputProps()}
              label={label}
              name={name}
              placeholder={placeholder}
              fullWidth
            />
            <MenuList {...props.getMenuProps()} className={classes.menuList}>
              {props.isOpen &&
                items
                  .filter(item => getItemSelected(item, props.inputValue))
                  .map((item, index) => (
                    <MenuItem
                      selected={props.highlightedIndex === index}
                      key={`${item}${index}`}
                      {...props.getItemProps({
                        item,
                        index,
                      })}
                    >
                      {item[itemLabel]}
                    </MenuItem>
                  ))}
            </MenuList>
          </Container>
        )
      }}
    </Downshift>
  )
}
