import React from 'react'
import { AutoSuggest, AutoSuggestProps } from './Autosuggest'
import { useField } from 'formik'

export function AutoSuggestField<T>(props: AutoSuggestProps<T>) {
  const [_, meta, helper] = useField(props.name)
  return (
    <AutoSuggest
      {...props}
      onChange={item => {
        helper.setValue((item && item[props.itemLabel]) || meta.initialValue)
      }}
      initialInputValue={meta.initialValue}
    />
  )
}
